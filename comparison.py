#!/usr/bin/env python3

from glob import glob
import yaml
from pprint import pprint
import os
import sys

os.chdir(sys.argv[1])

# determined manually by going through the long list with the local
# (geizhals.at) shop finder. tablets were not considered, and some devices
# might have dropped out for arbitrary other reasons (probably being phablets
# or overly expensive)
available = "Z00L tenshi paella chaozu m216 lux harpia athene klte a5y17lte yuga suzuran sumire crackling ham tulip mido".split()

data = [yaml.safe_load(open(a)) for a in glob("*.yml")]

def compare(attribute, evaluator=lambda x:x, extractor=None):
    print("\nComparing", attribute)
    if extractor is None:
        extractor = lambda x: x.get(attribute, None)

    levels = {}
    for a in data:
        evald = evaluator(extractor(a))
        levels.setdefault(evald, []).append(a)

    for i, (l, members) in enumerate(sorted(levels.items(), key=lambda a: (len(a[1]), repr(type(a[0])), a[0]))):
        if len(levels) - 1 == i:
            print(l, "all %d others" % len(members))
        else:
            print(l, ", ".join("%s (%s)" % (m['codename'], m['name']) for m in members))

compare('versions', lambda x: max(x))
compare('ram')
compare('architecture', evaluator=str)
compare('screen_tech')
def mm_from_composite(s):
    if s is None or s is False:
        return None
    if isinstance(s, list) and s and isinstance(s[0], dict):
        print("Warnining: Multiple sizes for a device, arbitrarily picking one")
        s = list(s[0].values())[0]
    try:
        return float(s.split('mm')[0])
    except Exception as e:
        print("Error when trying to parse %r" % (s,))
        raise
compare('height', evaluator=mm_from_composite, extractor = lambda x: isinstance(x.get('dimensions'), dict) and x['dimensions'].get('height'))
compare('width', evaluator=mm_from_composite, extractor = lambda x: isinstance(x.get('dimensions'), dict) and x['dimensions'].get('width'))
#compare('channels', evaluator=tuple)


#pprint(data[-2])


import pylab
data = [d for d in (yaml.safe_load(open(x)) for x in glob("*.yml")) if d['type'] != 'tablet']
sizes = [(
        mm_from_composite(d['dimensions']['width']),
        mm_from_composite(d['dimensions']['height']),
        'black' if d['codename'] in available else 'silver',
        3 if d['codename'] == 'suzuran' else 0.5,
        d['codename']
    ) for d in data if isinstance(d.get('dimensions'), dict) and d['dimensions'].get('width')]
x, y, available, was_chosen, codename = zip(*sizes)
pylab.axes().set_aspect('equal')
pylab.scatter(x, y, c=available, s=was_chosen)
pylab.scatter([63], [123], c='red', s=2) # my old zte blade III phone which had cyanogenmod 4.4
pylab.xlim(0)
pylab.ylim(0)
pylab.show()
